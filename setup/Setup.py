import logging

from selenium import webdriver
from selenium.webdriver.chrome.options import Options as COptions
from selenium.webdriver.firefox.options import Options as FOptions
from selenium.webdriver.ie.options import Options as IEOptions


class Setup:
    path = 'C://WebDriver'
    driver = None
    logging.basicConfig(level=logging.INFO)
    logger = logging.getLogger(' SETUP ')

    def chrome(self):
        chrome_options = COptions()
        chrome_options.add_argument("--headless")
        self.path += '//chromedriver.exe'
        self.driver = webdriver.Chrome(
            executable_path=self.path, options=chrome_options)

    def firefox(self):
        firefox_options = FOptions()
        firefox_options.add_argument("--headless")
        self.path += '//geckodriver.exe'
        self.driver = webdriver.Firefox(
            executable_path=self.path, firefox_options=firefox_options)

    def explorer(self):
        ie_options = IEOptions()
        ie_options.add_argument("--headless")
        self.path += '//IEDriverServer.exe'
        self.driver = webdriver.Ie(
            executable_path=self.path, ie_options=ie_options)

    def get_driver(self):
        return self.driver

    opciones = {'ch': chrome, 'ie': explorer, 'ff': firefox}

    def __init__(self, opcion):
        try:
            self.opciones[opcion](self)
            self.logger.info("Se creó un WebDriver")
        except Exception:
            self.logger.info("Error! Se ingresó una opción "
                             "no disponible para crear un WebDriver")
