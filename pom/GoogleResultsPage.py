import logging
import string

from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.support.wait import WebDriverWait


# Clase de la página de resultados de Google
# author: Javier Adalid
class GoogleResultsPage():
    driver = None
    results = None
    query = None
    logging.basicConfig(level=logging.INFO)
    logger = logging.getLogger(' GOOGLE RESULTS ')
    totalResults = 0

    # Constructor que recibe como parámetro un WebDriver
    def __init__(self, driver: webdriver):
        self.driver = driver

    # Método que obtiene todos los resultados de la búsqueda
    def get_results(self):
        try:
            wait = WebDriverWait(self.driver, 10)
            self.results = wait.until(
                ec.presence_of_all_elements_located
                ((By.XPATH, "//h3[@class='LC20lb DKV0Md']")))
            for result in self.results:
                if (result.text):
                    self.totalResults += 1
                    print(result.text)
                    print("-------------------------------------------------")
        except NoSuchElementException:
            self.logger.info("No hubo ningún resultado de búsqueda")
            return

    # Revisa si existe una sugerencia o corrección en la búsqueda
    # y realiza un click en ella en caso de que exista.
    def check_suggestion(self, iterations=0):
        click = None
        try:
            wait = WebDriverWait(self.driver, 10)
            wait.until(ec.visibility_of_element_located((By.ID, "result-stats")))
            suggestion = self.driver.find_element_by_xpath(
                "//a[@class='gL9Hy']")
            self.query=suggestion.text
            suggestion.click()
            click = True
            self.logger.info("Se hizo un click en la sugerencia/corrección")
            self.totalResults = 0
            self.check_suggestion(1)
        except NoSuchElementException:
            click = False
            if (iterations == 0):
                self.logger.info("No hay ninguna sugerencia/corrección")
        return click

    # Verifica que cada resultado tenga una descripción
    def verify_description(self):
        descriptions = self.driver.find_elements_by_xpath(
            "//span[@class='st' and normalize-space(text())]")
        totalDescriptions = len(descriptions)
        return (totalDescriptions, self.totalResults)

    # Verifica que el título del primer resultado contenga alguna de
    # las palabras proporcionadas en la búsqueda.
    def verify_first_result_title(self):
        global flag
        flag = False
        if(self.query == None):
            self.query = self.driver.find_element_by_xpath(
             "//input[@class='gLFyf gsfi']"). \
                get_attribute('value')
        splittedQuery = self.query.split()
        firstRes = self.results[0].text.lower()
        tildes, normal = 'áéíóúü', 'aeiouu'
        translation = firstRes.maketrans(tildes, normal)
        firstResult = firstRes.translate(translation)
        for str in splittedQuery:
            if str.lower() in firstResult:
                flag = True
                self.logger.info("El primer resultado contiene"
                                 " alguna de las palabras buscadas.")
        return flag

