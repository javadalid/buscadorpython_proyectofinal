import logging
import unittest

from pom.GoogleResultsPage import GoogleResultsPage
from pom.GoogleSearchPage import GoogleSearchPage
from setup.Setup import Setup


# Clase Google test que utiliza a unittest para la
# ejecución del caso de prueba.
# author: Javier Adalid.
class Test_Google(unittest.TestCase):
    # Declaraciones necesarias para el test
    logging.basicConfig(level=logging.INFO)
    logger = logging.getLogger(' MAIN TEST ')
    googleSearchPage = None
    googleResultsPage = None
    driver = None

    @classmethod
    def setUpClass(self) -> None:
        chromeDriver = Setup('ch')
        self.driver = chromeDriver.get_driver()
        self.googleSearchPage = GoogleSearchPage(self.driver)
        self.googleResultsPage = GoogleResultsPage(self.driver)
        self.googleSearchPage.get_google_homepage()
        self.googleSearchPage.google_search()
        self.googleResultsPage.get_results()

    def test_check_suggestion(self):
        self.assertIsNotNone(self.googleResultsPage.check_suggestion(),
                             "Error en la comprobación de sugerencia de búsqueda")
        self.googleResultsPage.get_results()

    def test_verify_description(self):
        (results, descriptions) = self.googleResultsPage.verify_description()
        self.assertEqual(results, descriptions,
                         "No todos los resultados cuentan con descripción")
        self.logger.info("Todos los resultados cuentan con descripción.")

    def test_verify_title(self):
        self.assertTrue(self.googleResultsPage.verify_first_result_title(),
                        "El título no contiene ningún término buscado.")

    @classmethod
    def tearDownClass(self) -> None:
        self.driver.close()


if __name__ == '__main__':
    unittest.main()
